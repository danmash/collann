from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer

from apps.annotation.utils import get_data_set_file_data


class AppConsumer(JsonWebsocketConsumer):
    def __init__(self, *args, **kwargs):
        self.set_hash = args[0]['url_route']['kwargs']['hash']
        self.groups = ['group_for_data_set_%s' % self.set_hash]
        super().__init__(*args, **kwargs)

    def receive_json(self, content, **kwargs):
        content['set_hash'] = self.set_hash
        for group in self.groups:
            async_to_sync(self.channel_layer.group_send)(group, content)

    def data_get_file_chunk(self, content):
        ds_data = get_data_set_file_data(content['set_hash'],
                                         content['start'],
                                         content['end'])
        self.send_json(dict(content, **ds_data))

    def data_set_selection(self, content):
        self.send_json(content)

    # Receive message from room group
    def chat_new_message(self, content):
        self.send_json(content)


        # http://weaver.nlplab.org/~brat/demo/latest/#/not-editable/BioNLP-ST_2011_ID_devel/PMC1804205-02-Results-01
        # todo; такую структуру нужно  поддерживать?
        # вот все сущности, создай http://brat.nlplab.org/configuration.html обычный
        # редакто admin? или модный?
        # тут еще перечитай
        # https://channels.readthedocs.io/en/latest/tutorial/part_2.html
