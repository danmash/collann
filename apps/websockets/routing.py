from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
    url(r'^ws/(?P<hash>\w{8})$', consumers.AppConsumer),
]