from rest_framework.routers import DefaultRouter

from apps.annotation.views import DataSetsViewSet

router = DefaultRouter()
router.register(r'data-sets', DataSetsViewSet, base_name='data-sets')
urlpatterns = router.urls
