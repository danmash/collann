from rest_framework.viewsets import ModelViewSet

from apps.annotation.models import DataSet
from apps.annotation.serializers import DataSetSerializer


class DataSetsViewSet(ModelViewSet):
    queryset = DataSet.objects.all().order_by('-created')
    serializer_class = DataSetSerializer

