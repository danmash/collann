from rest_framework.serializers import ModelSerializer, CharField

from apps.annotation.models import DataSet


class DataSetSerializer(ModelSerializer):
    class Meta:
        model = DataSet
        fields = '__all__'

    def to_internal_value(self, data):
        values = super().to_internal_value(data)
        values['name'] = values['source_file'].name
        return values

    def to_representation(self, instance):
        representation = super(DataSetSerializer, self).to_representation(instance)
        date_format = '%d-%m-%Y %H:%M'
        representation['created'] = instance.created.strftime(date_format)
        representation['modified'] = instance.modified.strftime(date_format)
        return representation