from django.contrib import admin

from apps.annotation.models import DataSet, EntityType, Entity


@admin.register(DataSet)
class DataSetAdmin(admin.ModelAdmin):
    exclude = ('chars_count_by_line',)


@admin.register(Entity)
class EntityAdmin(admin.ModelAdmin):
    pass


@admin.register(EntityType)
class EntityTypeAdmin(admin.ModelAdmin):
    pass
