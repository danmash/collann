from hashlib import md5

from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.annotation.models import DataSet


@receiver(post_save, sender=DataSet)
def calculate_data_set_hash(sender, instance, **kwargs):
    need_save = False
    if not instance.set_hash:
        s = '%sSALT-paiofjd' % instance.id
        instance.set_hash = md5(s.encode('utf-8')).hexdigest()
        need_save = True

    if not instance.chars_count_by_line:
        chars_from_start = 0
        chars_from_start_by_line = []
        for line in instance.source_file.file:
            chars_from_start += len(line)
            chars_from_start_by_line.append(chars_from_start)
        instance.chars_count_by_line = chars_from_start_by_line
        need_save = True

    if need_save:
        instance.save()
