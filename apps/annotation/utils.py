from itertools import islice

from apps.annotation.models import DataSet


def get_data_set_file_data(set_hash, start, end):
    data_set = DataSet.objects.get(set_hash=set_hash)
    byte_lines = list(islice(data_set.source_file.file, start, end))
    data_set.source_file.file.seek(0)
    return {'data': [b.decode('utf-8') for b in byte_lines],
            'hidden': data_set.chars_count_by_line[start]}

