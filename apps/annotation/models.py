from django.db import models
from django_extensions.db.fields import CreationDateTimeField, \
    ModificationDateTimeField

from apps.common.fields import TruncatingCharField, SeparatedValuesField


# TODO: help_text migration
class DataSet(models.Model):
    created = CreationDateTimeField('created')
    modified = ModificationDateTimeField('modified')
    name = models.CharField(max_length=128, blank=True, null=True)
    set_hash = TruncatingCharField(max_length=8, blank=True, null=True,
                                   default='')
    # chars from start by line
    chars_count_by_line = SeparatedValuesField(default='', blank=True, null=True)
    notes = models.TextField(blank=True)
    source_file = models.FileField(upload_to='data/')
    owner = models.ForeignKey('auth.User', on_delete=models.CASCADE,
                              blank=True, null=True)


class EntityType(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True)


class Entity(models.Model):
    entity_type = models.ForeignKey(EntityType, on_delete=models.CASCADE)
    name = models.CharField(max_length=128, blank=True, null=True)


from apps.annotation import signals
