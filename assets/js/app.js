import Vue from 'vue';
import Vuikit from 'vuikit'
import VuikitIcons from '@vuikit/icons'
import '@vuikit/theme'
import App from "./components/App.vue";
import VueRouter from 'vue-router'
import {routes} from './routes.js'
import VueCookie from 'vue-cookie'
const router = new VueRouter({
  mode: 'history',
  routes
});
Vue.use(VueRouter);
Vue.use(VueCookie);
Vue.use(Vuikit);
Vue.use(VuikitIcons);

const app = new Vue({
  el: '#app',
  router,
  components: {
    App
  }
});