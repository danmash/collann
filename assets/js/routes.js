import Home from './components/data_sets/Home.vue'
import Edit from './components/data_sets/Edit.vue'
import NotFound from './components/NotFound.vue'

export const routes = [
  { path: '/data/:hash', component: Edit, name: 'edit-data-set'},
  { path: '/', component: Home},
  {
    path: '/404',
    name: '404',
    component: NotFound,
  }, {
    path: '*',
    redirect: '/404'
  }
];